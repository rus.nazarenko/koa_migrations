// const { user, address } = require('../../models/index')
const db = require('../../db/models/index')

export const addAddressQuery = async (data) => {
  try {
    // const currentUser = await db.user.findOne({ where: { id: userId } })
    // if (!currentUser) throw new Error("No user")
    const result = await db.address.create(data)
    // await currentUser.setAddresses(result)
    return result.dataValues
  } catch (err) {
    throw new Error(err)
  }
}

export const delAddressQuery = async (data) => {
  try {
    const result = await db.address.destroy({ where: { id: data } })
    if (result) return
    throw new Error("No address")
  } catch (err) {
    throw new Error(err)
  }
}

export const updateAddressQuery = async (data) => {
  try {
    const result = await db.address.update(data, {
      where: { id: data.id }
    })
    if (result) return
    throw new Error("No address")
  } catch (err) {
    throw new Error(err)
  }
}

export const getAddressesQuery = async () => {
  try {
    const result = await db.address.findAll()
    return result
  } catch (err) {
    throw new Error(err)
  }
}


// CREATE TABLE IF NOT EXISTS "addresses" (
//   "id"   SERIAL , 
//   "country" VARCHAR(255), 
//   "city" VARCHAR(255), 
//   "street" VARCHAR(255), 
//   "building" VARCHAR(255), 
//   "created_at" TIMESTAMP WITH TIME ZONE NOT NULL, 
//   "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL, 
//   "user_id" INTEGER REFERENCES "users" ("id") ON DELETE SET NULL ON UPDATE CASCADE, 
//   PRIMARY KEY ("id"));

// CREATE TABLE IF NOT EXISTS "users" (
//   "id"   SERIAL , 
//   "user_name" VARCHAR(255), 
//   "role" VARCHAR(255), 
//   "email" VARCHAR(255), 
//   "password" VARCHAR(255), 
//   "created_at" TIMESTAMP WITH TIME ZONE NOT NULL, 
//   "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL, PRIMARY KEY ("id"));

// CREATE TABLE IF NOT EXISTS "addresses" (
//   "id"   SERIAL , 
//   "country" VARCHAR(255), 
//   "city" VARCHAR(255), 
//   "street" VARCHAR(255), 
//   "building" VARCHAR(255), 
//   "created_at" TIMESTAMP WITH TIME ZONE NOT NULL, 
//   "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL, 
//   "user_id" INTEGER REFERENCES "users" ("id") ON DELETE SET NULL ON UPDATE CASCADE, 
//   PRIMARY KEY ("id"));

// CREATE TABLE IF NOT EXISTS "users" (
//   "id"   SERIAL , 
//   "user_name" VARCHAR(255), 
//   "role" VARCHAR(255), 
//   "email" VARCHAR(255), 
//   "password" VARCHAR(255), 
//   "created_at" TIMESTAMP WITH TIME ZONE NOT NULL, 
//   "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL, PRIMARY KEY ("id"));
