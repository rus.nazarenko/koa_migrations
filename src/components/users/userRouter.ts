import * as Router from 'koa-router'
import {
  addUserController, delUserController, updateUserController, getUsersController
} from './userController'

const userRouter = new Router({ prefix: '/user' });


userRouter.post('/', addUserController)
userRouter.del('/:id', delUserController)
userRouter.put('/:id', updateUserController)
userRouter.get('/', getUsersController)

export default userRouter;