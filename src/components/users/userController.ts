import { Context } from 'koa'
import { addUserQuery, delUserQuery, updateUserQuery, getUsersQuery } from './userService'

export const addUserController = async (ctx: Context) => {
  try {
    if (Object.entries(ctx.request.body).length === 0) {
      throw new Error("No data")
    }

    const data = {
      name: ctx.request.body.user_name,
      role: ctx.request.body.role,
      password: ctx.request.body.password,
      email: ctx.request.body.email
    }

    const address_id = ctx.request.body.address_id

    const resAddUser = await addUserQuery(data, address_id)
    ctx.status = 201
    ctx.body = { success: true, data: resAddUser }
  } catch (error) {
    ctx.body = { success: false, error: error.message }
    ctx.status = 401
  }
}

export const delUserController = async (ctx: Context) => {
  try {
    const data = [ctx.params.id]
    await delUserQuery(data)
    ctx.status = 204
  } catch (error) {
    ctx.body = { success: false, error: error.message }
    ctx.status = 401
  }
}

export const updateUserController = async (ctx: Context) => {
  try {
    if (Object.entries(ctx.request.body).length === 0) {
      throw new Error("No data")
    }
    const data = {
      id: ctx.params.id,
      name: ctx.request.body.user_name,
      role: ctx.request.body.role,
      password: ctx.request.body.password,
      email: ctx.request.body.email
    }

    console.log("updateUserController", data)

    await updateUserQuery(data)
    ctx.status = 204
  } catch (error) {
    ctx.body = { success: false, error: error.message }
    ctx.status = 401
  }
}

export const getUsersController = async (ctx: Context) => {
  try {
    const resGetUsers = await getUsersQuery()
    ctx.status = 200
    ctx.body = { success: true, data: resGetUsers }
  } catch (error) {
    ctx.body = { success: false, error: error.message }
    ctx.status = 401
  }
}