const db = require('../../db/models/index')

export const addUserQuery = async (data, address_id) => {
  try {
    const currenAddress = await db.address.findOne({ where: { id: address_id } })
    if (!currenAddress) throw new Error("No address")
    // const result = await db.user.create({ ...data, address_id })
    const result = await db.user.create(data)
    await currenAddress.setUsers(result)
    await result.reload()
    return result.dataValues
  } catch (err) {
    throw new Error(err)
  }
}

export const delUserQuery = async (data) => {
  try {
    const result = await db.user.destroy({ where: { id: data } })
    if (result) return
    throw new Error("No user")
  } catch (err) {
    throw new Error(err)
  }
}

export const updateUserQuery = async (data) => {
  try {
    const result = await db.user.update(data, {
      where: { id: data.id }
    })
    if (result) return
    throw new Error("No user")
  } catch (err) {
    throw new Error(err)
  }
}

export const getUsersQuery = async () => {
  try {
    const result = await db.user.findAll()
    return result
  } catch (err) {
    throw new Error(err)
  }
}

// CREATE TABLE IF NOT EXISTS "addresses" (
//   "id"   SERIAL , 
//   "country" VARCHAR(255), 
//   "city" VARCHAR(255), 
//   "street" VARCHAR(255), 
//   "building" VARCHAR(255), 
//   "created_at" TIMESTAMP WITH TIME ZONE NOT NULL, 
//   "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL, PRIMARY KEY ("id"));

// CREATE TABLE IF NOT EXISTS "users" (
//   "id"   SERIAL , 
//   "user_name" VARCHAR(255), 
//   "role" VARCHAR(255), 
//   "email" VARCHAR(255), 
//   "password" VARCHAR(255), 
//   "created_at" TIMESTAMP WITH TIME ZONE NOT NULL, 
//   "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL, 
//   "address_id" INTEGER REFERENCES "addresses" ("id") ON DELETE SET NULL ON UPDATE CASCADE, 
//   PRIMARY KEY ("id"));