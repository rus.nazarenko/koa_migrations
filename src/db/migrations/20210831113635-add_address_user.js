'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('users', 'address_id', {
      type: Sequelize.INTEGER,
      references: {
        model: 'addresses',
        key: 'id'
      },
      allowNull: true,
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE'
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('users', 'address_id');
  }
}