'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    static associate(models) {
      this.belongsTo(models.address)
    }
  }

  user.init(
    {
      user_name: { type: DataTypes.STRING },
      role: { type: DataTypes.STRING },
      email: { type: DataTypes.STRING },
      password: { type: DataTypes.STRING }
    },
    {
      sequelize,
      modelName: 'user',
      underscored: true
    }
  )

  return user
}