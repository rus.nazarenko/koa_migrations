'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class address extends Model {
    static associate(models) {
      this.hasMany(models.user)
    }
  };
  address.init(
    {
      country: DataTypes.STRING,
      city: DataTypes.STRING,
      street: DataTypes.STRING,
      building: DataTypes.STRING
    },
    {
      sequelize,
      modelName: 'address',
      underscored: true
    });
  return address;
};