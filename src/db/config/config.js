
module.exports = {
  // {
  //   "development": {
  //     "database": "postgres",
  //     "dialect": "postgres",
  //     "password": "db_password",
  //     "username": "db_user",
  //     "host": "database"
  //   }
  // },
  'development': {
    'username': process.env.POSTGRES_USER,
    'password': process.env.POSTGRES_PASSWORD,
    'database': process.env.POSTGRES_DB,
    'seederStorageTableName': 'sequelize_data',
    'seederStorage': 'sequelize',
    'host': process.env.DB_HOST,
    'port': process.env.DB_PORT,
    'dialect': 'postgres',
    pool: {
      max: 20,
      min: 1
    }
  },
  'stage': {
    'username': process.env.POSTGRES_USER,
    'password': process.env.POSTGRES_PASSWORD,
    'database': process.env.POSTGRES_DB,
    'host': process.env.DB_HOST,
    'dialect': 'postgres'
  },
  'production': {
    'username': process.env.POSTGRES_USER,
    'password': process.env.POSTGRES_PASSWORD,
    'database': process.env.POSTGRES_DB,
    'host': process.env.DB_HOST,
    'dialect': 'postgres'
  }
};